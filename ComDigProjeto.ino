int carry = 0;
String msg = "";

void setup() {
  Serial.begin(9600);
  configInicial();
  configMsg();
 
}

void loop() {
  carryGen(carry);
  Serial.println(".");

  int *ones;
  ascToBinary(7, ones);
  Serial.println(*ones);
  delay(1000);
}

void configInicial() {
  Serial.println("Defina a frequencia da portadora");
  while(carry < 10)
    carry = Serial.parseInt();
  Serial.print("Portadora: ");
  Serial.println(carry);
}

void configMsg() {
  Serial.println("Defina a mensagem");
  while(msg == "")
    msg = Serial.readString();
  Serial.print("Mensagem: ");
  Serial.println(msg);
}


void carryGen(int pwm) {
  pinMode(3, OUTPUT);
  TCCR2A = 0x23;
  TCCR2B = 0x09;
  OCR2A = 3;
  OCR2B = 1;
}


//void readSerial() {
//  if (Serial.available() > 0) {
//    incomingByte = Serial.read();
//    Serial.print("I received: ");
//    Serial.println(incomingByte, DEC);
//  }
//}

